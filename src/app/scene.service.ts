import { Injectable } from '@angular/core';
import { GameService } from './game.service';
import {  GameModel } from './character';
import { AngularFireObject, AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class SceneService {
  sceneId:number;
  name:string;
  isHost:boolean;
  
  game:AngularFireObject<GameModel>;

  editableGame :GameModel;


  constructor(private db:AngularFireDatabase, private gameService:GameService) {
    this.name = this.gameService.name;
    
  }

  intialize()
  {
    this.isHost = this.gameService.GetLocalRoom(this.sceneId).host === this.name;
    this.game = this.db.object(`games/${this.sceneId}`)
    this.game.valueChanges().subscribe((newModel)=>{
      this.editableGame = newModel;
      this.isHost = newModel.host === this.name;
    });
  }

  updateGame()
  {
    this.game.set(this.editableGame);
  }


  


 
}
