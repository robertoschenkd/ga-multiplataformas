import { Component, OnInit } from '@angular/core';
import { GameScene } from '../character';
import { SceneService } from '../scene.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { GameService } from '../game.service';

@Component({
  selector: 'app-game-component',
  templateUrl: './game-component.component.html',
  styleUrls: ['./game-component.component.css']
})

export class GameComponentComponent implements OnInit {

  constructor(public sceneService: SceneService, public route: ActivatedRoute) { }

  public game: Phaser.Game;

  gameConfig: GameConfig = {
    type: Phaser.AUTO,
    width: 640,
    height: 480,
  };


  public onGameReady(game: Phaser.Game): void {
    this.game = game;
    this.sceneService.sceneId = +this.route.snapshot.paramMap.get('roomId');
    this.game.scene.add('GameScene', new GameScene(this.sceneService), true);
  }

  ngOnInit() {
    console.log('lole');

  }

}
