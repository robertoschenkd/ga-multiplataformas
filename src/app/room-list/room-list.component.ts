import { Component, OnInit, Inject, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { GameService } from '../game.service';
import { GameRoom } from '../Game/GameRoom';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class RoomListComponent implements OnInit, OnDestroy{
  ngOnDestroy(): void {
    this.updateSub.unsubscribe();
  }

  updateSub:Subscription;
  rooms: GameRoom[];
  name: string;

  constructor(private router: Router, public gameService: GameService, private changeDetector:ChangeDetectorRef) {
  }


  ngOnInit() {
    this.updateSub = this.gameService.GetRooms().subscribe((newRoom) => {  this.rooms = newRoom; this.changeDetector.detectChanges(); console.log(this.rooms) });
    this.name = this.gameService.GetName();
  }

  updateName()
  {
    this.gameService.SetName(this.name);
  }

  enterRoom(id)
  {
    this.gameService.EnterRoom(id, this.gameService.name);
    this.router.navigate([`/room/${id}`]);
  }
  createRoom() {
   

      this.gameService.CreateRoom(this.gameService.GetName(), "fi_desert").toPromise()
        .then((gameRoom) => {
          this.router.navigate([`/room/${gameRoom.id}`]);
        });

   
  }

 



}
