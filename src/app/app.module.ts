import { ApplicationRef, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import { PhaserModule } from 'phaser-component-library';

import { AppComponent } from './app.component';
import { GameComponentComponent } from './game-component/game-component.component';
import { RoomListComponent } from './room-list/room-list.component';
import { RoomCreationComponent } from './room-creation/room-creation.component';
import { RoomListItemComponent } from './room-list-item/room-list-item.component';
import { RoomViewComponent } from './room-view/room-view.component';
import { AppRoutingModule } from './/app-routing.module';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFireDatabaseModule } from '@angular/fire/database';

/**
 * Application module.
 */
@NgModule({
  declarations: [
    AppComponent,
    GameComponentComponent,
    RoomListComponent,
    RoomCreationComponent,
    RoomListItemComponent,
    RoomViewComponent
  ],
  imports: [
    BrowserModule,
    PhaserModule,
    AppRoutingModule,
    FormsModule ,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  /**
   * Instantiate application module.
   *
   * @param appRef Application reference, needed for [HMR](../hmr.ts).
   */
  public constructor(public appRef: ApplicationRef) { }
}
