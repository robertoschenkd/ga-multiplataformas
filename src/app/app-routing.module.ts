import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router'
import { RoomListComponent } from './room-list/room-list.component';
import { RoomCreationComponent } from './room-creation/room-creation.component';
import { RoomViewComponent } from './room-view/room-view.component';
import { GameComponentComponent } from './game-component/game-component.component';

const routes:Routes=[
  {path:'roomList', component:RoomListComponent},
  {path:'createRoom', component:RoomCreationComponent},
  {path:'room/:id', component:RoomViewComponent},
  {path:'game/:roomId', component:GameComponentComponent},
  {path:'', redirectTo:'roomList', pathMatch:"full"}
]

@NgModule({
  exports: [
    RouterModule
  ],
  imports:[RouterModule.forRoot(routes)],
  declarations: []
})


export class AppRoutingModule { 
  
}
