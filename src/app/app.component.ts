import { Component } from '@angular/core';
import { Router } from '@angular/router';


/**
 * Application component.
 */
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})

export class AppComponent {



  public constructor(public router:Router) { }

}
