import { Injectable } from '@angular/core';
import { Observable, of, ObservableLike, Subject } from 'rxjs';
import { GameRoom } from './Game/GameRoom';
import { Router } from '@angular/router';
import { AngularFireDatabase,  AngularFireObject} from '@angular/fire/database'
import { GameModel } from './character';

@Injectable({
  providedIn: 'root'
})
export class GameService {
 
  constructor(private db:AngularFireDatabase, private router:Router) { 
    db.database.ref('rooms').on("value", (snapshot)=>{
      this.rooms = [];
      snapshot.forEach((data)=>
      {
        this.rooms.push(data.val());
      });
     })
  }

  rooms : GameRoom[] ;

  name:string = "";

  StartGame(id: number):Promise<void> {
    
    return this.db.database.ref(`rooms/${id}/inGame`).set(true).then(
      ()=>{
        return this.db.database.ref(`games/${id}`).set(new GameModel(this.GetLocalRoom(id).players, this.name));
      }
    );
  }

  SetName(name: string):Promise<string> {
    this.name = name;
    return Promise.resolve<string>(this.name);
  }

  GetName()
  {
    return this.name;
  }

  mockGenerateRoom(name:string , map):GameRoom
  {
      let newRoom: GameRoom = new GameRoom();
     newRoom.id = Math.floor(Math.random() * 100);
     newRoom.host = name;
     newRoom.mapName = map;
     newRoom.players = [name];

     return newRoom;
  }

  GetRooms() : Observable<GameRoom[]>
  {
    return this.db.list<GameRoom>('rooms').valueChanges();
  }

  GetRoom(id:number):Observable<GameRoom>
  {
    return this.db.object<GameRoom>(`rooms/${id}`).valueChanges();
  }

  GetLocalRoom(id:number)
  {
    return this.rooms.find(r=>r.id === id);
  }

  EnterRoom(id:number, name:string)
  {
    let players = [];
      this.db.database.ref(`rooms/${id}/players`).once("value", (data)=>
    {
      players = data.val();
      if(!players.includes(name))
      {
      players.push(name);
      this.db.database.ref(`rooms/${id}/players`).set(players);
      }
    })

  }

  DeleteRoom(id:number)
  {
    this.db.database.ref('rooms').child(id.toString()).remove();
  }

  ExitRoom(id:number)
  {
    let room:GameRoom = this.rooms.find((r)=>r.id === id);
    if(room.players.length == 1 || this.name === room.host)
    {
      this.DeleteRoom(room.id);
      this.router.navigate(['roomList']);
    }
    else
    {
     this.db.database.ref('rooms').child(id.toString()).child('players').once("value", (data)=>
     {
       let players: string[] = data.val();
       let index = players.indexOf(this.name);
      players.splice(index, 1);
      this.db.database.ref('rooms').child(id.toString()).child('players').set(players);
     });
    }

    this.router.navigate(['/roomList']);
   
  }

  CreateRoom(hostName:string, map:string) : Observable<GameRoom>
  {
    let newRoom:GameRoom = this.mockGenerateRoom(hostName, map);
    let fireObj = this.db.database.ref('rooms').child(newRoom.id.toString());
    fireObj.set(newRoom);
    return of(newRoom);
  }

}
