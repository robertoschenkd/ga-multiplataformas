import { Component, OnInit, Input, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { GameRoom } from '../Game/GameRoom';
import { GameService } from '../game.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-room-view',
  templateUrl: './room-view.component.html',
  styleUrls: ['./room-view.component.css']
})
export class RoomViewComponent implements OnInit, OnDestroy {
  
  inGame: boolean;
  ngOnDestroy(): void {
    this.updateSubscription.unsubscribe();
  }

  room:GameRoom = new GameRoom();

  canStart = false;

  updateSubscription:Subscription;

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private gameService:GameService,
    private detectorRef:ChangeDetectorRef) { }
    
  
  ngOnInit() {
    this.updateSubscription = this.gameService.GetRoom(+this.route.snapshot.paramMap.get('id')).subscribe((roomUpdated)=>{
      this.room = roomUpdated; 
      this.canStart = this.room.host === this.gameService.name && this.room.players.length > 1;
      this.inGame = this.room.inGame;
      if(this.inGame)
      {
        this.router.navigate([`game/${this.room.id}`])
      }
      console.log(this.room);
      this.detectorRef.detectChanges();
    });
    

  }

  exitRoom()
  {
    this.gameService.ExitRoom(this.room.id);
  }

  startGame()
  {
    this.gameService.StartGame(this.room.id).then(
      ()=>{
        console.log("WHAT");
      }
    )
  }

   


}
