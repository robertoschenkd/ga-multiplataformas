import { SceneService } from "./scene.service";
import { Observable } from "rxjs";
import { extend } from "webdriver-js-extender";
import { EventEmitter } from "events";

export class Character extends Phaser.GameObjects.Sprite {

    leftKey: Phaser.Input.Keyboard.Key;

    constructor(scene, x, y) {
        super(scene, x, y, "character");
        this.leftKey = this.scene.input.keyboard.addKey(new Phaser.Input.Keyboard.Key(Phaser.Input.Keyboard.KeyCodes.A));
        this.displayWidth = 32;
        this.displayHeight = 32;
    }
    update() {
        super.update();
        if (this.leftKey.isDown) {
            this.x -= 1;
        }
    }
}

class State {
    sceneService: SceneService;
    scene: GameScene;

    initialize(sceneService: SceneService, scene: GameScene) {
        this.sceneService = sceneService;
        this.scene = scene;
    }

    update() {

    }

    onClickTile(i, j, val) {
        console.log(i, j, val);
    }

    bindClick() {
        this.scene.mapView.bindClick((i, j, val) => { this.onClickTile(i, j, val); });
    }


}


class NeighborData {
    bombs: number;
    treasure: boolean;
    constructor() {
        this.bombs = 0;
        this.treasure = false;
    }
}

class EditingState extends State {

    bombs = 6;
    treasure = 1;


    initialize(sceneService: SceneService, scene: GameScene) {
        super.initialize(sceneService, scene);
        if (!this.sceneService.isHost) {
            this.scene.mapView.obfuscate(true);
        }
        else {
            this.bindClick();
        }
    }



    update() {
        if (this.sceneService.isHost) {
            if (this.bombs > 0) {
                this.scene.messageText.text = `Place the bombs. ${this.bombs} left`
            }
            else if (this.treasure > 0) {
                this.scene.messageText.text = `Place the treasure.`
            }
            else {

            }
        }
    }

    clearState() {
        this.scene.messageText.text = "";
    }

    changeTileValue(i, j) {
        if (this.bombs > 0) {
            this.sceneService.editableGame.gameMap[i][j] = 10;
            this.bombs--;
            this.sceneService.updateGame();

        }
        else if (this.treasure > 0) {
            this.sceneService.editableGame.gameMap[i][j] = 11;
            this.sceneService.editableGame.state = 'play';
            this.sceneService.updateGame();
            this.treasure--;
        }
    }

    onClickTile(i, j, val) {
        if (val == 0 && this.sceneService.isHost) {
            this.changeTileValue(i, j);
        }
    }

}

export class PlayState extends State {
    initialize(sceneService: SceneService, scene: GameScene) {
        super.initialize(sceneService, scene);
        this.scene.mapView.obfuscate(false);
        this.scene.mapView.gameMode = true;

        if (!this.sceneService.isHost) {
            this.bindClick();
            this.scene.messageText.text = "Try to find the treasure. ! means that you found the treasure's neighbor";
        }
        else {
            this.scene.messageText.text = "Now watch him suffer.";
        }



    }



    countAdjacent(startI, startJ): NeighborData {
        let neighborData = new NeighborData();

        for (let i = -1; i < 2; i++) {
            for (let j = -1; j < 2; j++) {
                let val = this.safeGetTileData(startI + i, startJ + j);
                console.log(`Invesitagting:${startI + i},${ startJ + j}:${val}`);
                if (val == 10) {
                    neighborData.bombs++;
                }
                else if (val == 11) {
                    neighborData.treasure = true;
                }

            }
        }

        return neighborData;

    }

    safeGetTileData(i, j): number {
        if (i > 0 && i < this.sceneService.editableGame.gameMap.length && j > 0 && j < this.sceneService.editableGame.gameMap[i].length) {
            return this.sceneService.editableGame.gameMap[i][j];
        }
        else {
            return 0;
        }
    }

    onClickTile(i, j, val) {
        console.log(`Clicked:${i} ${j} ${val}`);
        let playingVal = this.sceneService.editableGame.playingMap[i][j];
        let mapVal = this.sceneService.editableGame.gameMap[i][j];
        if (playingVal == -1 && mapVal == 0) {
            let neighbors = this.countAdjacent(i, j);
            this.sceneService.editableGame.playingMap[i][j] = neighbors.bombs;

            if (neighbors.treasure) {
                this.sceneService.editableGame.playingMap[i][j] = neighbors.bombs + 1000;
            }
            this.sceneService.updateGame();
        }
        else if (mapVal == 10) {
            this.sceneService.editableGame.state = 'hostWin';
            this.sceneService.editableGame.playingMap[i][j] = -10;
            this.sceneService.updateGame();

        }
        else if (mapVal == 11) {
            this.sceneService.editableGame.state = 'hostLoss';
            this.sceneService.editableGame.playingMap[i][j] = -11;
            this.sceneService.updateGame();
        }
    }

}

export class EndState extends State {
    initialize(sceneService: SceneService, scene: GameScene) {
        super.initialize(sceneService, scene);
        this.scene.mapView.gameMode = false;
        if (this.sceneService.editableGame.state == 'hostWin') {
            this.scene.messageText.text = this.sceneService.isHost ? "You Win!" : "You lose!";
        }
        else {
            this.scene.messageText.text = this.sceneService.isHost ? "You lose!" : "You Win!";
        }
    }
}

export class GameScene extends Phaser.Scene {
    sceneService: SceneService;
    messageText: Phaser.GameObjects.Text;
    mapView: MapView;
    currentState: State;

    onClickTile;


    public constructor(sceneService: SceneService) {
        super({ key: 'Scene' });
        this.sceneService = sceneService;
        this.sceneService.intialize();
    }

    preload() {
        this.load.image('character', '/src/assets/character.png');
    }

    create() {
        this.messageText = this.add.text(100, 300, "");
        this.mapView = new MapRawView(this, "mapView");
        this.add.existing(this.mapView);

        this.sceneService.game.valueChanges().subscribe((game) => {
            this.sceneService.editableGame = game;
            if (game.state == 'play') {
                this.ChangeState(new PlayState());
            }
            else if (game.state == 'hostWin' || game.state == 'hostLoss') {
                this.ChangeState(new EndState());

            }
        })


        this.ChangeState(new EditingState());


    }

    update(time, delta) {
        if (this.sceneService.editableGame != undefined && this.sceneService.editableGame.state != 'play') {
            this.mapView.updateMap(this.sceneService.editableGame.gameMap);
        }
        else if(this.sceneService.editableGame != undefined && this.sceneService.editableGame.state == 'play')
        {
            this.mapView.updateMap(this.sceneService.editableGame.playingMap);
        }
        
        this.currentState.update();

    }


    setMessageText(message: string) {
        this.messageText.setText(message);
    }

    ChangeState(newState: State) {
        this.currentState = newState;
        this.onClickTile = newState.onClickTile;
        this.currentState.initialize(this.sceneService, this);
    }

}

export class GameModel {
    state: string;
    gameMap: number[][];
    playingMap: number[][];
    players: string[];
    host:string;

    constructor(players: string[], host:string) {
        this.state = "editing";
        this.initializeMap();
        this.players = players;
        this.host = host;
    }

    initializeMap() {
        this.gameMap = [];
        this.playingMap = [];
        for (let i = 0; i < 5; i++) {
            this.gameMap.push([]);
            this.playingMap.push([]);
            for (let j = 0; j < 5; j++) {
                this.gameMap[i].push(0);
                this.playingMap[i].push(-1);
            }
        }
    }

    Restart()
    {
        this.initializeMap();
        
    }
}



export class MapView extends Phaser.GameObjects.GameObject {

    gameModel: GameModel;
    obfuscating: boolean = false;
    clickEmitter: EventEmitter;
    gameMode: boolean = false;
    

    bindClick(clickCallback) {
        this.clickEmitter = new EventEmitter();
        this.clickEmitter.addListener('click', function (i, j, data) { clickCallback(i, j, data) });
    }

    updateMap(game: number[][]) { };

    setTreasureNeighbor(i, j) {

    }

    getRepresentation(val: number) {
        if (this.obfuscating) {
            return '*'
        }
        else if (this.gameMode) {
            if(val >= 1000)
            {
                return "!" + (val - 1000).toString();
            }
            else if (val > 0 && val < 10) {
                return val.toString();
            }
            else {
               return '*';
            }
        }
        else {
            return val.toString();
        }

    }


    obfuscate(value: boolean) {
        this.obfuscating = value;
    };


}

export class MapRawView extends MapView {
    texts: Phaser.GameObjects.Text[][] = null;

    setTreasureNeighbor(i, j) {
        this.texts[i][j].setColor("green");
    }

    updateMap(gameMap: number[][]) {
        console.log(this.obfuscating);
        if (this.texts === null) {
            this.texts = [];
            for (let i = 0; i < gameMap.length; i++) {
                this.texts.push([]);
                for (let j = 0; j < gameMap.length; j++) {
                    let text: Phaser.GameObjects.Text = this.scene.add.text(32 * j, 32 * i, this.getRepresentation(gameMap[i][j]));
                    text.setInteractive();
                    text.input.enabled = true;

                    text.on('pointerdown', () => {
                        let currentI = i;
                        let currentJ = j;
                        console.log(this.clickEmitter);
                        this.clickEmitter.emit('click', currentI, currentJ, gameMap[currentI][currentJ]);
                    });

                    this.texts[i].push(text);
                }
            }
        }
        else {
            for (let i = 0; i < gameMap.length; i++) {
                for (let j = 0; j < gameMap.length; j++) {
                    let val = gameMap[i][j];
                    this.texts[i][j].setText(this.getRepresentation(val));
                }
            }
        }

    }


}